#include <mainwindow.h>
#include <ui_mainwindow.h>
#include <QDebug>
#include <QMessageBox>
#include <QFile>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);

  connect(ui->scanButton, SIGNAL(clicked()), this, SLOT(scanClicked()));
  connect(ui->camComboBox, SIGNAL(currentIndexChanged(int)), this,
          SLOT(camComboIndexChanged(int)));
  connect(ui->queryButton, SIGNAL(clicked()), this, SLOT(queryClicked()));
  connect(ui->defaultsButton, SIGNAL(clicked()), this, SLOT(defaultsClicked()));
  connect(ui->rebootButton, SIGNAL(clicked()), this, SLOT(rebootClicked()));
  connect(ui->treeWidget, SIGNAL(itemDoubleClicked(QTreeWidgetItem*, int)), this, SLOT(itemDoubleClicked(QTreeWidgetItem*, int)));
  connect(ui->bootCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(bootSettingChanged(int)));
  connect(ui->llaButton, SIGNAL(clicked()), this, SLOT(setLLAClicked()));
  connect(ui->dhcpButton, SIGNAL(clicked()), this, SLOT(setDHCPClicked()));
  connect(ui->staticButton, SIGNAL(clicked()), this, SLOT(setStaticClicked()));
  arv_debug_enable ("device:3");

  this->updateCamInfo(-1);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::clear()
{
    this->device = nullptr;
    this->deviceIndex = -1;

    this->cameraNames.clear();
    this->ui->treeWidget->clear();
    this->ui->camComboBox->clear();
}

void MainWindow::scanClicked() {
  qDebug() << "Scan clicked";
  this->scan();
}

void MainWindow::camComboIndexChanged(int) {
    this->ui->treeWidget->clear();
 }

void MainWindow::queryClicked()
{
    qDebug() << "Query clicked";
    int index = this->ui->camComboBox->currentIndex();
    this->updateCamInfo(index);
    this->buildGenICamTree(index);
    this->updateCamInfo(index);

    this->ui->treeWidget->header()->resizeSection(0, 280);

    this->ui->bootCombo->setEnabled(true);
    this->ui->saveAButton->setEnabled(true);
    this->ui->saveBButton->setEnabled(true);

    QString boot(arv_device_get_string_feature_value(this->device, "UserSetDefault"));

    if (boot == "Default") {
        this->ui->bootCombo->setCurrentIndex(0);
    } else if (boot == "UserSet1") {
        this->ui->bootCombo->setCurrentIndex(1);
    } else {
        this->ui->bootCombo->setCurrentIndex(2);
    }
}

void MainWindow::itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    qDebug() << "DOUBLE CLICKED: " << item->text(0) << column;
    QString feature(item->text(0));

    if (feature.contains("GevCurrent")) {
        QMessageBox::warning(this, "Error", "You are not allowed to edit ephemeral IP information", QMessageBox::Ok);
        return;
    }
    if (feature.contains("GevPersistent")) {
        QMessageBox::warning(this, "Error", "You are not allowed to edit static IP information individually. Use 'Set DHCP' in the main app.", QMessageBox::Ok);
        return;
    }

    ArvGc *genicam = arv_device_get_genicam(device);
    ArvGcNode *node = arv_gc_get_node(genicam, feature.toLocal8Bit().data());

    if (ARV_IS_GC_CATEGORY(node)) {
        return; // don't do anything for categories
    } else {
        if (this->editor) {
            delete this->editor;
        }
        this->editor = new FieldEditor(this);
        this->editor->buildUI(feature, node);
        this->editor->show();
    }
}

void MainWindow::defaultsClicked()
{
    arv_device_set_string_feature_value(this->device, "UserSetSelector", "Default");
    arv_device_execute_command(this->device, "UserSetLoad");
    this->ui->treeWidget->clear();
}

void MainWindow::rebootClicked()
{
    arv_device_execute_command(this->device, "DeviceReset");
    this->clear();
}

void MainWindow::saveAClicked()
{
    this->save(1);
}

void MainWindow::saveBClicked()
{
    this->save(2);
}

void MainWindow::bootSettingChanged(int idx)
{
    switch (idx){
    case 0:
        arv_device_set_string_feature_value(this->device, "UserSetDefault", "Default");
        break;
    case 1:
        arv_device_set_string_feature_value(this->device, "UserSetDefault", "UserSet1");
        break;
    case 2:
        arv_device_set_string_feature_value(this->device, "UserSetDefault", "UserSet2");
        break;
    }
}

void MainWindow::setLLAClicked()
{
    int c = arv_get_n_devices();
    for (int i = 0; i < c; i++){
        if(i == this->ui->camComboBox->currentIndex() && this->device) {
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationLLA", true);
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationDHCP", false);
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationPersistentIP", false);
            arv_device_execute_command(this->device, "UserSetSave");
        }
        ArvDevice* d = arv_open_device(arv_get_device_id(i));
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationLLA", true);
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationDHCP", false);
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationPersistentIP", false);
        arv_device_execute_command(d, "UserSetSave");
    }
}

void MainWindow::setDHCPClicked()
{
    int c = arv_get_n_devices();
    for (int i = 0; i < c; i++){
        if(i == this->ui->camComboBox->currentIndex() && this->device) {
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationLLA", true);
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationDHCP", true);
            arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationPersistentIP", false);
            arv_device_execute_command(this->device, "UserSetSave");
        }
        ArvDevice* d = arv_open_device(arv_get_device_id(i));
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationLLA", true);
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationDHCP", true);
        arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationPersistentIP", false);
        arv_device_execute_command(d, "UserSetSave");
    }
}

void MainWindow::setStaticClicked()
{
    long ipbase = 168430181;
    ipeditor ipwin(&ipbase, this);
    ipwin.exec();
    qDebug() << "CLOSED!";

    long gw = ipbase - (ipbase % 256) + 1;
    long sub = 4294967040;

    if (ipbase != -1) {

        qDebug() << "SETTING...!";
        int c = arv_get_n_devices();
        for (int i = 0; i < c; i++){
            if(i == this->ui->camComboBox->currentIndex() && this->device) {
                arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationLLA", true);
                arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationDHCP", false);
                arv_device_set_boolean_feature_value(this->device, "GevCurrentIPConfigurationPersistentIP", true);
                arv_device_set_integer_feature_value(this->device, "GevPersistentIPAddress", ipbase + i);
                arv_device_set_integer_feature_value(this->device, "GevPersistentSubnetMask", sub);
                arv_device_set_integer_feature_value(this->device, "GevPersistentDefaultGateway", gw);
                arv_device_execute_command(this->device, "UserSetSave");
            }
            ArvDevice* d = arv_open_device(arv_get_device_id(i));
            arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationLLA", true);
            arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationDHCP", false);
            arv_device_set_boolean_feature_value(d, "GevCurrentIPConfigurationPersistentIP", true);
            arv_device_set_integer_feature_value(d, "GevPersistentIPAddress", ipbase + i);
            arv_device_set_integer_feature_value(d, "GevPersistentSubnetMask", sub);
            arv_device_set_integer_feature_value(d, "GevPersistentDefaultGateway", gw);
            arv_device_execute_command(d, "UserSetSave");
        }
    } else {
        qDebug() << "CANCELLED!";
    }
}

void MainWindow::save(int set)
{
    if (set == 1) {
        arv_device_set_string_feature_value(this->device, "UserSetSelector", "UserSet1");
    } else {
        arv_device_set_string_feature_value(this->device, "UserSetSelector", "UserSet2");
    }
    arv_device_execute_command(this->device, "UserSetSave");
}

void MainWindow::scan() {
    this->clear();
  arv_update_device_list();
  this->ncams = arv_get_n_devices();

  this->ui->lcdNumber->display(QString::number(this->ncams));

  for (uint i = 0; i < this->ncams; i++) {
    std::string camName = arv_get_device_id(i);
    camName = camName + " " + arv_get_device_serial_nbr(i);
    this->cameraNames.push_back(camName);
  }

  this->updateCamList();

  int c = this->cameraNames.size();
  if (c == 0) {
    ui->camInfoLabel->setText("No cameras detected");
    this->setButtonEnabled(false);
  } else {
      this->updateCamInfo(0);
    this->setButtonEnabled(true);
  }
}

void MainWindow::setButtonEnabled(bool enabled)
{

    this->ui->queryButton->setEnabled(enabled);
    this->ui->defaultsButton->setEnabled(enabled);
    this->ui->rebootButton->setEnabled(enabled);
    this->ui->llaButton->setEnabled(enabled);
    this->ui->dhcpButton->setEnabled(enabled);
    this->ui->staticButton->setEnabled(enabled);
}

void MainWindow::updateCamInfo(int idx) {
  QString msg;


  if(!this->device){
      msg = "Click Scan";
      ui->camInfoLabel->setText(msg);
      return;
  }

  if (idx == -1) {
    msg = "Click Scan";
  } else {
    msg = "Physical Address:";
    msg = msg + QString(arv_get_device_physical_id(idx));
    msg = msg + "\nAddress: " + QString(arv_get_device_address(idx));
    msg = msg + "\nSerial Number: " + QString(arv_get_device_serial_nbr(idx));
  }

  ui->camInfoLabel->setText(msg);
}

void MainWindow::buildGenICamTree(int idx) {
    this->deviceIndex = idx;
    const char* deviceid = arv_get_device_id(this->deviceIndex);
  this->device = arv_open_device(deviceid);
  ArvGc *genicam = arv_device_get_genicam(device);


  this->root = new QTreeWidgetItem(this->ui->treeWidget);

  this->root->setText(0, arv_get_device_serial_nbr(idx));
  this->root->setText(1, this->cameraNames[idx].c_str());

  ArvGcNode *node = arv_gc_get_node(genicam, "Root");

  const GSList *features;
  const GSList *iter;

  features = arv_gc_category_get_features(ARV_GC_CATEGORY(node));

  for (iter = features; iter != NULL; iter = iter->next) {
    QString feature(static_cast<char *>(iter->data));
    this->addTreeChild(this->root, genicam, feature);
  }
}

void MainWindow::updateCamList() {
  for (auto i = this->cameraNames.begin(); i < this->cameraNames.end(); i++) {
    ui->camComboBox->addItem(i->c_str());
  }
}

void MainWindow::addTreeChild(QTreeWidgetItem *parent, void *gc,
                              QString feature) {
  ArvGc *genicam = static_cast<ArvGc *>(gc);
  QTreeWidgetItem *item = new QTreeWidgetItem();
  ArvGcNode *node = arv_gc_get_node(genicam, feature.toLocal8Bit().data());
  const char *description =
      arv_gc_feature_node_get_description(ARV_GC_FEATURE_NODE(node), NULL);

  item->setText(0, feature);
  item->setText(1, description);

  parent->addChild(item);

  int fieldtype = this->editor->getFieldType(node);

  switch(fieldtype) {
  case -1: //category
  case 0: // enum
  case 1: // command
  case 2: //int
  case 3: //float
  case 4: //bool
  case 5: //string
      break;
  default:
      qDebug() << "WEIRD:" << feature << fieldtype;
      break;
  }


  if (ARV_IS_GC_CATEGORY(node)) {
    const GSList *features;
    const GSList *iter;

    features = arv_gc_category_get_features(ARV_GC_CATEGORY(node));

    for (iter = features; iter != NULL; iter = iter->next) {
      QString feature(static_cast<char *>(iter->data));
      this->addTreeChild(item, genicam, feature);
    }
  }
}
