#ifndef IPEDITOR_H
#define IPEDITOR_H

#include <QDialog>

namespace Ui {
class ipeditor;
}

class ipeditor : public QDialog
{
    Q_OBJECT

public:
    explicit ipeditor(long* ip, QWidget *parent = nullptr);
    ~ipeditor();

public slots:
    void rejected();
    void accepted();

private:
    Ui::ipeditor *ui;
    long* ip;
};

#endif // IPEDITOR_H
