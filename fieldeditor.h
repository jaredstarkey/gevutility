#ifndef FIELDEDITOR_H
#define FIELDEDITOR_H

#include "aravis-0.6/arv.h"
#include <QDialog>
#include <QMainWindow>
#include <QTreeWidget>
#include <vector>
#include "ui_fieldeditor.h"

class MainWindow;

namespace Ui {
class FieldEditor;
}  // namespace Ui

class FieldEditor : public QDialog {
    Q_OBJECT

public:
    explicit FieldEditor(MainWindow *parent = nullptr);
    ~FieldEditor();
    int getFieldType(ArvGcNode *node);

public slots:
    void saveClicked();
    void cancelClicked();
    void executeClicked();
    void closeEvent(QCloseEvent* event);


    void buildUI(QString, ArvGcNode*);

private:
    Ui::QFieldEditor *ui;
    int fieldtype = -1;
    ArvDevice* device;
    int index = -1;
    QWidget* thewidget;
    MainWindow* mainwin;
};


#endif // FIELDEDITOR_H
