#include "fieldeditor.h"
#include "mainwindow.h"
#include "ui_fieldeditor.h"
#include <QDebug>
#include <QPushButton>
#include <QLineEdit>
#include <QComboBox>

FieldEditor::FieldEditor(MainWindow *parent)
    : QDialog(parent), ui(new Ui::QFieldEditor), mainwin(parent) {
    ui->setupUi(this);

    connect(ui->buttonBox, SIGNAL(accepted()), this, SLOT(saveClicked()));
    connect(ui->buttonBox, SIGNAL(rejected()), this,
            SLOT(cancelClicked()));

    this->device = parent->device;
    this->index = parent->deviceIndex;

}

FieldEditor::~FieldEditor() { this->ui->~QFieldEditor(); delete this->ui; }

void FieldEditor::saveClicked()
{

    bool allcameras = this->ui->applyToAllCamerasCheckBox->isChecked();
    QString value;
    QString feature;

    switch(this->fieldtype) {
    case -1: // category, ignore
        break;
    case 0: // enum
    case 5: //string
    {
        QComboBox* w = static_cast<QComboBox*>(this->thewidget);
        value = w->currentText();
        feature = this->ui->settingLineEdit->text();

        if (allcameras) {
            int c = arv_get_n_devices();
            for (int i = 0; i < c; i++){
                if(i == this->index) {
                    arv_device_set_string_feature_value(this->device, feature.toLocal8Bit().data(), value.toLocal8Bit().data());
                }
                ArvDevice* d = arv_open_device(arv_get_device_id(i));
                arv_device_set_string_feature_value(d, feature.toLocal8Bit().data(), value.toLocal8Bit().data());
            }
        } else {
            arv_device_set_string_feature_value(this->device, feature.toLocal8Bit().data(), value.toLocal8Bit().data());
        }

        break;
    }
    break;
    case 1: { // command
        //nothing
        break;
    }
    case 2: //int
    case 3: //float
    {
        QLineEdit* w = static_cast<QLineEdit*>(this->thewidget);
        feature = this->ui->settingLineEdit->text();

        switch(this->fieldtype) {
        case 2:
        {
            value = w->text();
            int val = value.toInt();

            if (allcameras) {
                int c = arv_get_n_devices();
                for (int i = 0; i < c; i++){
                    if(i == this->index) {
                        arv_device_set_integer_feature_value(this->device, feature.toLocal8Bit().data(), val);
                    }
                    ArvDevice* d = arv_open_device(arv_get_device_id(i));
                    arv_device_set_integer_feature_value(d, feature.toLocal8Bit().data(), val);
                }
            } else {
                arv_device_set_integer_feature_value(this->device, feature.toLocal8Bit().data(), val);
            }
        }
            break;
        case 3:
            {
                value = w->text();
                float val = value.toFloat();

                if (allcameras) {
                    int c = arv_get_n_devices();
                    for (int i = 0; i < c; i++){
                        if(i == this->index) {
                            arv_device_set_float_feature_value(this->device, feature.toLocal8Bit().data(), val);
                        }
                        ArvDevice* d = arv_open_device(arv_get_device_id(i));
                        arv_device_set_float_feature_value(d, feature.toLocal8Bit().data(), val);
                    }
                } else {
                    arv_device_set_float_feature_value(this->device, feature.toLocal8Bit().data(), val);
                }
                break;
            }
        break;
        }
    }
    break;
    case 4: //bool
    {
        QComboBox* w = static_cast<QComboBox*>(this->thewidget);
        value = w->currentText();
        feature = this->ui->settingLineEdit->text();
        bool val = (value[0] == "T") ? true : false;

        if (allcameras) {
            int c = arv_get_n_devices();
            for (int i = 0; i < c; i++){
                if(i == this->index) {
                    arv_device_set_boolean_feature_value(this->device, feature.toLocal8Bit().data(), val);
                }
                ArvDevice* d = arv_open_device(arv_get_device_id(i));
                arv_device_set_boolean_feature_value(d, feature.toLocal8Bit().data(), val);
            }
        } else {
            arv_device_set_boolean_feature_value(this->device, feature.toLocal8Bit().data(), val);
        }

        break;
    }
    default:
        qDebug() << "WEIRD SAVE";
        break;
    }
}

void FieldEditor::cancelClicked()
{
    this->ui->formLayout->removeRow(2);
    this->fieldtype = -1;
    this->index = -1;
    this->device = nullptr;
    this->close();
}

void FieldEditor::executeClicked()
{
    qDebug() << "EXECUTE";
    arv_device_execute_command(this->device, this->ui->settingLineEdit->text().toLocal8Bit().data());
}

void FieldEditor::closeEvent(QCloseEvent *)
{
    this->cancelClicked();
}


int FieldEditor::getFieldType(ArvGcNode *node)
{
    if (ARV_IS_GC_ENUMERATION(node)) {
        return 0;
    } else if (ARV_IS_GC_CATEGORY(node)) {
        return -1;
    } else if (ARV_IS_GC_COMMAND(node)) {
        return 1;
    } else if (ARV_IS_GC_BOOLEAN(node)) {
        return 4;
    } else if (ARV_IS_GC_INTEGER(node)) {
        return 2;
    } else if (ARV_IS_GC_FLOAT(node)) {
        return 3;
    } else if (ARV_IS_GC_STRING(node)) {
        return 5;
    } if (ARV_IS_GC_SWISS_KNIFE(node)) {
        return 6;
    } else {
        return 7;
    }

}
void FieldEditor::buildUI(QString feature, ArvGcNode* node)
{
    if (this->fieldtype != -1) {
        //window is already open...ignore
        return;
    }

    this->ui->settingLineEdit->setText(feature);
    this->fieldtype = this->getFieldType(node);

    qDebug() << "BUILDING FORM FOR " << feature << fieldtype;

    QString value;

    switch(this->fieldtype) {
    case -1: // category, ignore
        break;
    case 0: // enum
        {
            QComboBox* edit = new QComboBox(this);
            this->thewidget = edit;
            value = arv_device_get_string_feature_value(device, feature.toLocal8Bit().data());
            guint numvals = 0;
            int selval = 0;
            const char ** entries = arv_gc_enumeration_get_available_string_values(ARV_GC_ENUMERATION(node), &numvals, nullptr);
            for (uint i = 0; i < numvals; i++) {
                edit->addItem(entries[i]);
                if (entries[i] == value) {
                    selval = i;
                }
            }
            edit->setCurrentIndex(selval);
            this->ui->formLayout->insertRow(2, "New Value:", this->thewidget);
            break;
        }
        break;
    case 1: { // command
        QPushButton* button = new QPushButton("Execute", this);
        this->thewidget = button;
        connect(button, SIGNAL(clicked()), this, SLOT(executeClicked()));
        this->ui->formLayout->insertRow(2, button);
        value = "NA - Node is a command";
        break;
    }
    case 2: //int
    case 3: //float
    case 5: //string
    {
        QLineEdit* edit = new QLineEdit(this);
        this->thewidget = edit;
        this->ui->formLayout->insertRow(2, "New Value:", edit);

        switch(this->fieldtype) {
        case 2:
            value = QString::number(arv_device_get_integer_feature_value(device, feature.toLocal8Bit().data()));
            break;
        case 3:
            value = QString::number(arv_device_get_float_feature_value(device, feature.toLocal8Bit().data()));
            break;
        case 4:
            value = arv_device_get_string_feature_value(device, feature.toLocal8Bit().data());
            break;
        }
        edit->setText(value);

        break;
    }
        break;
    case 4: //bool
    {
        QComboBox* edit = new QComboBox(this);
        this->thewidget = edit;
        edit->addItem("True");
        edit->addItem("False");
        this->ui->formLayout->insertRow(2, "New Value:", edit);
        value = arv_device_get_string_feature_value(device, feature.toLocal8Bit().data());
        break;
    }
    default:
        qDebug() << "WEIRD:" << feature << fieldtype;
        break;
    }

    this->ui->currentValueLineEdit->setText(value);

}
