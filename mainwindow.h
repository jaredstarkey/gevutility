#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "aravis-0.6/arv.h"
#include <QMainWindow>
#include <QTreeWidget>
#include <vector>
#include "fieldeditor.h"
#include "ipeditor.h"

namespace Ui {
class MainWindow;
}  // namespace Ui

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  ArvDevice* device = nullptr;
  int deviceIndex = -1;

  void clear();
  void save(int);

 public slots:
  void scanClicked();
  void camComboIndexChanged(int idx);
  void queryClicked();
  void itemDoubleClicked(QTreeWidgetItem*, int);
  void defaultsClicked();
  void rebootClicked();
  void saveAClicked();
  void saveBClicked();
  void bootSettingChanged(int);
  void setLLAClicked();
  void setDHCPClicked();
  void setStaticClicked();


 private:
  Ui::MainWindow *ui;

  void scan();
  void setButtonEnabled(bool enabled);
  void updateCamInfo(int);
  void buildGenICamTree(int idx);
  void addTreeChild(QTreeWidgetItem *parent, void *genicam, QString feature);

  //////////////////////////////
  uint ncams;
  std::vector<std::string> cameraNames;

  QTreeWidgetItem *root = nullptr;

  void updateCamList();

  FieldEditor* editor = nullptr;

  int cString = 0;
  int cInt = 0;
  int cFloat = 0;
  int cBool = 0;
  int cEnum = 0;
  int cCommand = 0;
  int cRegister = 0;
  int cOther = 0;
  int cSwiss = 0;
};

#endif  // MAINWINDOW_H
