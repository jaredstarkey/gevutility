#include "ipeditor.h"
#include "ui_ipeditor.h"
#include <math.h>

ipeditor::ipeditor(long* ipb, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ipeditor), ip(ipb)
{
    ui->setupUi(this);

    this->ip = ipb;

    connect(this->ui->buttonBox, SIGNAL(accepted()), this, SLOT(accepted()));
    connect(this->ui->buttonBox, SIGNAL(rejected()), this, SLOT(rejected()));

    int octets[4] = {
        static_cast<int>(floor(*ipb / 16777216)),
        static_cast<int>(floor((*ipb % 16777216) / 65536)),
        static_cast<int>(floor((*ipb % 65536) / 256)),
        static_cast<int>(floor(*ipb % 256))
    };

    QString txt[4] = {
        QString::number(octets[0]),
        QString::number(octets[1]),
        QString::number(octets[2]),
        QString::number(octets[3])
    };

    this->ui->aEdit->setText(txt[0]);
    this->ui->bEdit->setText(txt[1]);
    this->ui->cEdit->setText(txt[2]);
    this->ui->dEdit->setText(txt[3]);
}

ipeditor::~ipeditor()
{
    delete ui;
}

void ipeditor::rejected()
{
    *(this->ip) = -1;
    this->close();
}

void ipeditor::accepted()
{
    this->close();
}
